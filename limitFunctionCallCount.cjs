//==================== Problem 2 =====================================================



// function limitFunctionCallCount(cb, n) {
//     // Should return a function that invokes `cb`.
//     // The returned function should only allow `cb` to be invoked `n` times.
//     // Returning null is acceptable if cb can't be returned
// }


//==============================================================================



function limitFunctionCallCount(callBack, n) {
        let counterNumber = 0
        if(typeof callBack !== "function"){
            throw new Error("missing or wrong first arguments instead of function.");
        }else if(typeof n !== "number"){
            throw new Error("missing or wrong type of second arguments instead of number.");
        }

        return (...allArguments) => {
            // console.log(allArguments);
            if (counterNumber < n) {
                counterNumber++;
                return callBack(...allArguments);
            } else {
                return null;
            }
        }
}



module.exports = limitFunctionCallCount;



