//========================================== Problem3 ===========================================


// function cacheFunction(cb) {
//     // Should return a function that invokes `cb`.
//     // A cache (object) should be kept in closure scope.
//     // The cache should keep track of all arguments have been used to invoke this function.
//     // If the returned function is invoked with arguments that it has already seen
//     // then it should return the cached result and not invoke `cb` again.
//     // `cb` should only ever be invoked once for a given set of arguments.
// }



//===============================================================================================


function cacheFunction(callBack) {
    if (typeof callBack !== "function") {
        throw new Error('missing or wrong parameters passed instead of a callback function !')
    }

    const cache = {};
    return function (...allArguments) {
        // callBack()

        const argsKey = JSON.stringify(allArguments);
        // console.log(argsKey);
        if (argsKey in cache) {
            // console.log(argsKey);
            // console.log('sadfsdfadf');
            return cache[argsKey];
        }
        const callBackResult = callBack(...allArguments);
        cache[argsKey] = callBackResult;
        // console.log(cache);

        return callBackResult;
    }



}



module.exports = cacheFunction;











