
const problem2 = require('./limitFunctionCallCount.cjs');

try {
    function cb(name) {
        console.log(`my name is ${name}`);
        return;
    }
    const name = problem2(cb,4);
    // const name = problem2("sdfas",4);

    name("Zuber");
    name();
    name("Santhu");
    name("Faizan");
    name("ssssss");
    name("aaaaa");
    name("mmmmm");
}catch(error){
    console.log(error);
}